# Subway Smell

![Codeship Status](https://www.codeship.io/projects/988d42f0-3f29-0131-9b06-7e537d7e4452/status)

### Requirements

* [virtualenvwrapper](http://virtualenvwrapper.readthedocs.org/en/latest/)

### Setup

1. fork and/or clone
2. `cd path/to/project`
3. `mkvirtualenv subwaysmell`
4. `setvirtualenvproject`
5. `pip install -r requirements.txt`
6. To run dev server: `python subwaysmell.py`